import bigInt from 'big-integer';

const letters = 'acdegilmnoprstuw';

export const hash = (s) => {
  let h = 7;
  for (let i = 0; i < s.length; i++) {
    h = (h * 37 + letters.indexOf(s[i]))
  }
  return h;
};

export const hashBigInt = (s) => {
  let h = bigInt('7');
  for (let i = 0; i < s.length; i++) {
    h = h.multiply(37).plus(letters.indexOf(s[i]))
  }
  return h;
};

// The hash function doesn't actually destroy the original data so we can reverse the process to
// decode the data.
export const unhash = (hash) => {
  let s = [];
  while (hash > 7) {
    // Find the remainder
    const i = hash % 37;

    // The letters are in reverse order so we add them to the front of the array
    s.unshift(letters[i]);

    // Reverse one iteration of the hash
    hash = (hash - i)/37;
  }
  return s.join('');
};

// The hash function doesn't actually destroy the original data so we can reverse the process to
// decode the data.
export const unhashBigInt = (hash) => {
  let s = [];
  while (hash > 7) {
    // Find the remainder
    const i = hash.mod(37);

    // The letters are in reverse order so we add them to the front of the array
    s.unshift(letters[i]);

    // Reverse one iteration of the hash
    hash = hash.minus(i).divide(37);
  }
  return s.join('');
};

// // Increment number with respect to any base
// export const increment = (digits, base) => {
//   // Iterate through digits from left to right
//   for (let i = digits.length - 1; i >= 0; i--) {
//     // Need to carry to next digit?
//     if (digits[i] === base - 1) {
//       // Zero out and cary to next digit
//       digits[i] = 0;
//     } else {
//       // Increment
//       digits[i]++;
//       break;
//     }
//   }
//   return digits;
// }
//
// export const digitsToString = (digits) => {
//   return digits.map(digit => letters[digit]).join('');
// }
//
// export const unhashBruteForce = (hashed, length) => {
//   const base = letters.length;
//
//   // Initialize digits
//   let digits = Array(length).fill(0);
//
//   // Total posibilities
//   const n = Math.pow(letters.length, length);
//
//   // Use brute force to generate all possible strings and then check to see if the hash matches
//   for (let i = 0; i < n; i++) {
//     const str = digitsToString(digits);
//     if (hash(str) === hashed) {
//       return str;
//     } else {
//       digits = increment(digits, base);
//     }
//   }
//
//   return null;
// }
