import { hash, hashBigInt, increment, unhashBruteForce, digitsToString, unhash, unhashBigInt } from './hash';
import bigInt from 'big-integer';

it('should hash', () => {
  expect(hash('leepadg')).toEqual(680131659347)

  // Sanity check with manual calculations
  expect(hash('a')).toEqual(259);
  expect(hash('aw')).toEqual(9598); // 259 * 37 + 15

  // Sanity check answers
  expect(hash('promenade')).toEqual(945924806726376);
  expect(hashBigInt('westernista')).toEqual(bigInt('1317985395604951854'));
});

it('should unhash', () => {
  expect(unhash(259)).toEqual('a');
  expect(unhash(9598)).toEqual('aw');
})

// Note: takes 11ms
it('should unhash 680131659347', () => {
  expect(unhash(680131659347)).toEqual('leepadg');
})

it('should unhash 945924806726376', () => {
  expect(unhash(945924806726376)).toEqual('promenade');
})

it('should unhash 1317985395604951854', () => {
  expect(unhashBigInt(bigInt('1317985395604951854'))).toEqual('westernista');
})

// it('should increment number', () => {
//   expect(increment([0], 2)).toEqual([1])
//
//   expect(increment([0, 0], 3)).toEqual([0, 1])
//   expect(increment([0, 2], 3)).toEqual([1, 0])
//   expect(increment([1, 0], 3)).toEqual([1, 1])
//   expect(increment([1, 1], 3)).toEqual([1, 2])
//
//   expect(increment([0, 1, 0], 2)).toEqual([0, 1, 1])
//   expect(increment([0, 1, 1], 2)).toEqual([1, 0, 0])
// });
//
// it('should convert digits to string', () => {
//   expect(digitsToString([0, 0])).toEqual('aa');
//   expect(digitsToString([15, 2, 1])).toEqual('wdc');
// });
//
// it('should unhashBruteForce', () => {
//   expect(unhashBruteForce(259, 1)).toEqual('a');
//   expect(unhashBruteForce(9598, 2)).toEqual('aw');
// })
//
// // Note: takes 61236ms
// it.skip('should unhashBruteForce 680131659347', () => {
//   expect(unhashBruteForce(680131659347, 7)).toEqual('leepadg');
// })
//
// // Should take about 60secs * 16 * 16 = 256 mins
// it.skip('should unhashBruteForce 945924806726376', () => {
//   expect(unhashBruteForce(945924806726376, 9)).toEqual('mpromemade');
// })
